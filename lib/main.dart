import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ms2019_2/resources/instituicaoServ.dart';
import 'package:ms2019_2/resources/singleton.dart';
import 'package:ms2019_2/resources/usuarioServ.dart';
import 'package:ms2019_2/views/pages/instituicao/main/instituicaoMainPage.dart';
import 'package:ms2019_2/views/pages/login/mainLoginPage.dart';
import 'package:ms2019_2/views/pages/usuario/main/usuarioMainPage.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
    FirebaseAuth.instance.currentUser().then((user) {
        if(user != null) {
            InstituicaoServ.getInstituicao(user.email).then((inst) {
            if(inst != null) {
              Singleton.getInstance().instituicao = inst;
              return runApp(MyApp(1));
            } else {
              UsuarioServ.getUsuario(user.email).then((usu) {
                Singleton.getInstance().usuario = usu;
                return runApp(MyApp(2));
              });
            }
          });
        } else {
          return runApp(MyApp(0));
        }
    });
}
        

class MyApp extends StatelessWidget {
  final int logado;

  MyApp(this.logado);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      supportedLocales: [const Locale("pt", "BR")],
      title: 'Rangou',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: logado == 0 ? MainLoginPage() : logado == 1 ? InstituicaoMainPage() : UsuarioMainPage(),
    );
  }
}
