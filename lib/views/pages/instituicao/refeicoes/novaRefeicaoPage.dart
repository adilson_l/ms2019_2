import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:ms2019_2/models/refeicao.dart';
import 'package:ms2019_2/models/refeitorio.dart';
import 'package:ms2019_2/resources/refeicaoServ.dart';
import 'package:ms2019_2/resources/singleton.dart';

class NovaRefeicaoPage extends StatefulWidget {
  final Refeitorio refeitorio;
  final Refeicao refeicao;

  NovaRefeicaoPage(this.refeitorio, this.refeicao);

  @override
  _NovaRefeicaoPageState createState() => _NovaRefeicaoPageState();
}

class _NovaRefeicaoPageState extends State<NovaRefeicaoPage> {
  Refeicao _refeicao;
  List<String> _refeicoes = ["Café da manhã", "Almoço", "Café da tarde", "Jantar", "Outros"];

  @override
  void initState() {
    _refeicao = widget.refeicao == null ? Refeicao.nulo() : widget.refeicao;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.deepOrange,
        title: Text(
            (widget.refeicao == null ? "Cadastrar" : "Editar") + " Refeição"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.check),
            onPressed: () async {
              if(_refeicao.tipo != null) {
                  try {
                  if (widget.refeicao == null) {
                    await RefeicaoServ.addRefeicao(
                        _refeicao,
                        Singleton.getInstance().instituicao.docID,
                        widget.refeitorio.docID);
                  } else {
                    await RefeicaoServ.editRefeicao(
                        _refeicao,
                        Singleton.getInstance().instituicao.docID,
                        widget.refeitorio.docID);
                  }
                  Navigator.of(context).pop(true);
                } catch (e) {
                  Navigator.of(context).pop(false);
                }
              } else {
                _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Defina o tipo de refeição")));
              }
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            DropdownButton<String>(
              hint: Text(_refeicao.tipo ?? "Tipo"),
              items: _refeicoes.map((String refeicao) {
                return DropdownMenuItem<String>(
                  value: refeicao,
                  child: Text(refeicao),
                );
              }).toList(),
              onChanged: (refeicao) {
                _refeicao.tipo = refeicao;
                setState(() {});
              },
            ),
            ExpansionTile(
              title: Text("Segunda-Feira"),
              children: <Widget>[
                Container(
                  padding:
                      EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("Hora de Início: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.segInicio != null
                                  ? formatDate(
                                      _refeicao.segInicio, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.segInicio = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("   Hora de Fim: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.segFim != null
                                  ? formatDate(_refeicao.segFim, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.segFim = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("Hora máxima\npara check-in: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.segMax != null
                                  ? formatDate(_refeicao.segMax, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.segMax = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
            ExpansionTile(
              title: Text("Terça-Feira"),
              children: <Widget>[
                Container(
                  padding:
                      EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("Hora de Início: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.terInicio != null
                                  ? formatDate(
                                      _refeicao.terInicio, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.terInicio = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("   Hora de Fim: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.terFim != null
                                  ? formatDate(_refeicao.terFim, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.terFim = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("Hora máxima\npara check-in: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.terMax != null
                                  ? formatDate(_refeicao.terMax, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.terMax = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
            ExpansionTile(
              title: Text("Quarta-Feira"),
              children: <Widget>[
                Container(
                  padding:
                      EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("Hora de Início: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.quaInicio != null
                                  ? formatDate(
                                      _refeicao.quaInicio, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.quaInicio = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("   Hora de Fim: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.quaFim != null
                                  ? formatDate(_refeicao.quaFim, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.quaFim = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("Hora máxima\npara check-in: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.quaMax != null
                                  ? formatDate(_refeicao.quaMax, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.quaMax = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
            ExpansionTile(
              title: Text("Quinta-Feira"),
              children: <Widget>[
                Container(
                  padding:
                      EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("Hora de Início: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.quiInicio != null
                                  ? formatDate(
                                      _refeicao.quiInicio, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.quiInicio = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("   Hora de Fim: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.quiFim != null
                                  ? formatDate(_refeicao.quiFim, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.quiFim = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("Hora máxima\npara check-in: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.quiMax != null
                                  ? formatDate(_refeicao.quiMax, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.quiMax = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
            ExpansionTile(
              title: Text("Sexta-Feira"),
              children: <Widget>[
                Container(
                  padding:
                      EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("Hora de Início: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.sexInicio != null
                                  ? formatDate(
                                      _refeicao.sexInicio, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.sexInicio = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("   Hora de Fim: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.sexFim != null
                                  ? formatDate(_refeicao.sexFim, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.sexFim = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("Hora máxima\npara check-in: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.sexMax != null
                                  ? formatDate(_refeicao.sexMax, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.sexMax = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
            ExpansionTile(
              title: Text("Sábado"),
              children: <Widget>[
                Container(
                  padding:
                      EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("Hora de Início: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.sabInicio != null
                                  ? formatDate(
                                      _refeicao.sabInicio, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.sabInicio = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("   Hora de Fim: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.sabFim != null
                                  ? formatDate(_refeicao.sabFim, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.sabFim = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("Hora máxima\npara check-in: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.sabMax != null
                                  ? formatDate(_refeicao.sabMax, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.sabMax = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
            ExpansionTile(
              title: Text("Domingo"),
              children: <Widget>[
                Container(
                  padding:
                      EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("Hora de Início: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.domInicio != null
                                  ? formatDate(
                                      _refeicao.domInicio, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.domInicio = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("   Hora de Fim: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.domFim != null
                                  ? formatDate(_refeicao.domFim, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.domFim = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text("Hora máxima\npara check-in: "),
                          Container(
                            child: RaisedButton(
                              child: Text(_refeicao.domMax != null
                                  ? formatDate(_refeicao.domMax, [HH, ":", nn])
                                  : ""),
                              onPressed: () {
                                showTimePicker(
                                        context: context,
                                        initialTime:
                                            TimeOfDay(hour: 0, minute: 0))
                                    .then((time) {
                                  if (time != null) {
                                    _refeicao.domMax = DateTime(
                                        2000, 1, 1, time.hour, time.minute);
                                    setState(() {});
                                  }
                                });
                              },
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
