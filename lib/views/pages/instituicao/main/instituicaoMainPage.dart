import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ms2019_2/models/refeitorio.dart';
import 'package:ms2019_2/resources/singleton.dart';
import 'package:ms2019_2/views/pages/instituicao/main/cadastroRefeitorio.dart';
import 'package:ms2019_2/views/pages/instituicao/refeitorios/mainRefeitorios.dart';
import 'package:ms2019_2/views/pages/login/mainLoginPage.dart';

class InstituicaoMainPage extends StatefulWidget {
  @override
  _InstituicaoMainPageState createState() => _InstituicaoMainPageState();
}

class _InstituicaoMainPageState extends State<InstituicaoMainPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepOrange,
        title: Text("Refeitórios"),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: ()async{
              await FirebaseAuth.instance.signOut();
              Singleton.getInstance().instituicao=null;
              Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute(
                                            builder: (BuildContext context) => MainLoginPage()),
                                        (Route<dynamic> route) => false);
            },
          )
        ],
      ),
      body: StreamBuilder(
        stream: Firestore.instance
            .collection('instituicoes/' +
                Singleton.getInstance().instituicao.docID +
                '/refeitorios')
            .snapshots(),
        builder: (context, snaps) {
          switch (snaps.connectionState) {
            case ConnectionState.waiting:
            case ConnectionState.none:
              return CircularProgressIndicator();
              break;
            default:
              if (!snaps.hasData) return const Text('Connecting...');
              final int cardLength = snaps.data.documents.length;
              return ListView.builder(
                itemCount: cardLength,
                itemBuilder: (context, index) {
                  Refeitorio ref = Refeitorio.fromJson(snaps.data.documents[index].data, snaps.data.documents[index].documentID);
                  return GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => MainRefeitorios(ref)));
                    },
                    child: Card(
                      child: Container(
                        padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
                        height: MediaQuery.of(context).size.height * 0.08,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(ref.descricao, style: TextStyle(fontWeight: FontWeight.bold)),
                            Text(ref.localizacao)
                          ],
                        ),
                      ),
                    ),
                  );
                },
              );
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.deepOrange,
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => CadastroRefeitorioPage()));
        },
      ),
    );
  }
}
