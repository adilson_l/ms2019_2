import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ms2019_2/models/instituicao.dart';
import 'package:ms2019_2/models/refeitorio.dart';
import 'package:ms2019_2/models/vinculo.dart';

class RefeitorioServ{

  static Future<String> addRefeitorio(Refeitorio refeitorio,Instituicao instituicao)async{
    String retorno;
    try {
      
      var doc = await Firestore.instance.collection('instituicoes/${instituicao.docID}/refeitorios').add(refeitorio.toJson());
      Vinculo vinculo = Vinculo(instituicao: instituicao.docID, instituicaoNome: instituicao.nome, refeitorio: doc.documentID, refeitorioNome: refeitorio.descricao);
      var docVinculo = await Firestore.instance.collection('vinculos').add(vinculo.toJson());
      String docId = docVinculo.documentID;
      await Firestore.instance.document('vinculos/' + docId).updateData({"codigo": instituicao.nome.substring(0,3) + "-" + docId.substring(docId.length - 5, docId.length)});
      await Firestore.instance.document('instituicoes/${instituicao.docID}/refeitorios/' + doc.documentID).updateData({"codigoQR": instituicao.nome.substring(0,3) + "-" + docId.substring(docId.length - 5, docId.length)});
      retorno=doc.documentID;
    } catch (e) {
      retorno=e.toString();
    }
    return retorno;
  }

   static Future<bool> editRefeitorio(Refeitorio refeitorio,String instituicaoDocID)async{
    bool retorno;
    try {
      await Firestore.instance.collection('instituicoes/$instituicaoDocID/refeitorios').document(refeitorio.docID).updateData(refeitorio.toJson());
      retorno=true;
    } catch (e) {
      print(e.toString());
      retorno=false;
    }
    return retorno;
  }

  static Future<Refeitorio> getRefeitorio(String instituicaoID,String refeitorioID)async{
    Refeitorio retorno;
    var query = await Firestore.instance.document('instituicoes/$instituicaoID/refeitorios/$refeitorioID').get();
    if(query.data!=null && query.data.isNotEmpty){
      retorno=Refeitorio.fromJson(query.data,query.documentID);
    }

    return retorno;
  }


}