import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ms2019_2/models/instituicao.dart';

class InstituicaoServ{

  static Future<String> addInstituicao(Instituicao instituicao)async{
    String retorno;
    try {
      var doc = await Firestore.instance.collection('instituicoes').add(instituicao.toJson());
      retorno=doc.documentID;
    } catch (e) {
      retorno=e.toString();
    }
    return retorno;
  }

   static Future<bool> editInstituicao(Instituicao instituicao)async{
    bool retorno;
    try {
      await Firestore.instance.collection('instituicoes').document(instituicao.docID).updateData(instituicao.toJson());
      retorno=true;
    } catch (e) {
      print(e.toString());
      retorno=false;
    }
    return retorno;
  }

  static Future<Instituicao> getInstituicao(String email)async{
    
    Instituicao instituicao;

    try {
      QuerySnapshot querySnapshot = await Firestore.instance.collection('instituicoes').where('email',isEqualTo: email).getDocuments();

      if(querySnapshot.documents.isEmpty)
      {
        return null;
      }
      else
      {
        instituicao = Instituicao.fromJson(querySnapshot.documents.first.data, querySnapshot.documents.first.documentID);
        
        return instituicao;
      }

    } catch (e) {
      print(e.toString());

      return null;
    }

  }
}