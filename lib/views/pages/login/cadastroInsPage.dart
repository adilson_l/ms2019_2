import 'package:flutter/material.dart';
import 'package:ms2019_2/models/instituicao.dart';
import 'package:ms2019_2/resources/loginServ.dart';
import 'package:ms2019_2/views/components/fundoGradienteHot.dart';
import 'package:ms2019_2/views/pages/instituicao/main/instituicaoMainPage.dart';

class CadastroInstituicaoPage extends StatefulWidget {
  @override
  _CadastroInstituicaoPageState createState() => _CadastroInstituicaoPageState();
}

class _CadastroInstituicaoPageState extends State<CadastroInstituicaoPage> {
 GlobalKey<FormState> _key = GlobalKey();
  GlobalKey<ScaffoldState> _scafoldkey=GlobalKey();
  TextEditingController nomeController= TextEditingController();
  TextEditingController senhaController= TextEditingController();
  TextEditingController senha2Controller= TextEditingController();
  TextEditingController cnpjController= TextEditingController();
  TextEditingController emailController= TextEditingController();
  TextEditingController cepController= TextEditingController();
  TextEditingController cidadeController= TextEditingController();
  TextEditingController logadouroController= TextEditingController();
  TextEditingController numeroController= TextEditingController();
  TextEditingController estadoController= TextEditingController();
  
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scafoldkey,
     body: Stack(
       children: <Widget>[
         FundoGradienteHot(),
         Center(
              child: Form(
                key: _key,
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Text('Cadastrar',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 32
                      ),),
                      Container(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.05),
                        child: TextFormField(
                          style: TextStyle(color: Colors.white),
                          controller: nomeController,
                          validator: (value){
                            if(value==null || value.isEmpty){
                              return 'Campo inválido';
                            }else{
                              return null;
                            }
                          },
                          decoration: InputDecoration(
                            labelText: 'Nome',
                            labelStyle:TextStyle(color: Colors.white),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white
                              )
                            ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white))),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.01),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.05),
                        child: TextFormField(
                          style: TextStyle(color: Colors.white),
                          controller: emailController,
                          validator: (String valor) {
                    Pattern pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regex = new RegExp(pattern);
                    if (!regex.hasMatch(valor))
                      return 'Digite um e-mail válido';
                    else
                      return null;
                  },
                          decoration: InputDecoration(
                            labelText: 'Email',
                            labelStyle:TextStyle(color: Colors.white),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white
                              )
                            ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white))),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.01),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.05),
                        child: TextFormField(
                          controller: cnpjController,
                           validator: (value){
                            if(value==null || value.isEmpty){
                              return 'Campo inválido';
                            }else{
                              return null;
                            }
                          },
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                            labelText: 'CNPJ',
                            labelStyle:TextStyle(color: Colors.white),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white
                              )
                            ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white))),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.01),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.05),
                        child: TextFormField(
                          controller: logadouroController,
                           validator: (value){
                            if(value==null || value.isEmpty){
                              return 'Campo inválido';
                            }else{
                              return null;
                            }
                          },
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                            labelText: 'Logadouro',
                            labelStyle:TextStyle(color: Colors.white),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white
                              )
                            ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white))),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.01),
                      ),Container(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.05),
                        child: TextFormField(
                          controller: numeroController,
                           validator: (value){
                            if(value==null || value.isEmpty){
                              return 'Campo inválido';
                            }else{
                              return null;
                            }
                          },
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                            labelText: 'Numero',
                            labelStyle:TextStyle(color: Colors.white),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white
                              )
                            ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white))),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.01),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.05),
                        child: TextFormField(
                          controller: estadoController,
                           validator: (value){
                            if(value==null || value.isEmpty){
                              return 'Campo inválido';
                            }else{
                              return null;
                            }
                          },
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                            labelText: 'Estado',
                            labelStyle:TextStyle(color: Colors.white),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white
                              )
                            ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white))),
                        ),
                      ),
                      
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.01),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.05),
                        child: TextFormField(
                          controller: cidadeController,
                           validator: (value){
                            if(value==null || value.isEmpty){
                              return 'Campo inválido';
                            }else{
                              return null;
                            }
                          },
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                            labelText: 'Cidade',
                            labelStyle:TextStyle(color: Colors.white),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white
                              )
                            ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white))),
                        ),
                      ),
                      
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.01),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.05),
                        child: TextFormField(
                          controller: cepController,
                           validator: (value){
                            if(value==null || value.isEmpty){
                              return 'Campo inválido';
                            }else{
                              return null;
                            }
                          },
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                            labelText: 'CEP',
                            labelStyle:TextStyle(color: Colors.white),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white
                              )
                            ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white))),
                        ),
                      ),
                      
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.01),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.05),
                        child: TextFormField(
                          style: TextStyle(color: Colors.white),
                           validator: (value){
                            if(value==null || value.isEmpty|| value.length<6){
                              return 'Campo inválido';
                            }else{
                              return null;
                            }
                          },
                          controller: senhaController,
                          obscureText: true,
                          decoration: InputDecoration(
                            labelText: 'Senha',
                            labelStyle:TextStyle(color: Colors.white),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white
                              )
                            ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white))),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.01),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.05),
                        child: TextFormField(
                          controller: senha2Controller,
                           validator: (value){
                            if(value==null || value.isEmpty || value.length<6){
                              return 'Campo inválido';
                            }else{
                              return null;
                            }
                          },
                          obscureText: true,
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                            labelText: 'Repetir Senha',
                            labelStyle:TextStyle(color: Colors.white),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white
                              )
                            ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white))),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width*0.9,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(15.0)),
                          color: Colors.white,
                          child: Text("Cadastrar",
                          style: TextStyle(color: Colors.blueAccent),),
                          onPressed: ()async{
                            if(_key.currentState.validate()){
                              Instituicao instituicao=Instituicao(nomeController.text,
                               emailController.text, cidadeController.text, 
                               numeroController.text, logadouroController.text, cepController.text,
                                cnpjController.text, estadoController.text);
                               bool logou=await  LoginServ.signInWithEmailInstituicao(context, emailController.text, senhaController.text,
                                 instituicao, _scafoldkey);
                                 if(logou)
                                 {
                                   Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute(
                                            builder: (BuildContext context) => InstituicaoMainPage()),
                                        (Route<dynamic> route) => false);
                                 }
                            }else{
                                _scafoldkey.currentState.showSnackBar(SnackBar(
                                content: Text('Inválido'),
                              ));
                            }
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
       ],
     ),
      
    ),
    );
  }
}