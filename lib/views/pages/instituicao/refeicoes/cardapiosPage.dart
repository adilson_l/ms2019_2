import 'package:flutter/material.dart';
import 'package:ms2019_2/models/cardapio.dart';
import 'package:ms2019_2/models/refeicao.dart';
import 'package:ms2019_2/models/refeitorio.dart';
import 'package:ms2019_2/resources/cardapioServ.dart';
import 'package:ms2019_2/resources/singleton.dart';
import 'package:ms2019_2/views/pages/instituicao/refeicoes/checkin.dart';
import 'package:ms2019_2/views/pages/instituicao/refeicoes/novoCardapioPage.dart';
import 'package:table_calendar/table_calendar.dart';

class CardapiosPage extends StatefulWidget {
  final Refeitorio refeitorio;
  final Refeicao refeicao;

  CardapiosPage(this.refeitorio, this.refeicao);

  @override
  _CardapiosPageState createState() => _CardapiosPageState();
}

class _CardapiosPageState extends State<CardapiosPage> {
  bool _isBusy = false;
  Map<DateTime, List> _eventos = Map();
  List<Cardapio> _cardapios = List();
  Cardapio _selecionado;
  DateTime _dataSelecionada;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  CalendarController calendarController = CalendarController();

  @override
  void initState() {
    _isBusy = true;
    CardapioServ.getCardapios(Singleton.getInstance().instituicao.docID,
            widget.refeitorio.docID, widget.refeicao.docID)
        .then((cardapios) {
      _cardapios = cardapios;
      for (Cardapio cardapio in _cardapios) {
        if (_eventos[cardapio.data] == null) _eventos[cardapio.data] = List();
        _eventos[cardapio.data].add(cardapio);
      }
      _carregarEventos(DateTime.now());
      _isBusy = false;
      setState(() {});
    });
    super.initState();
  }

  void _carregarEventos(DateTime dia) {
    bool entrou = false;
    _eventos.forEach((data, listaCardapio) {
      if (data.day == dia.day &&
          data.month == dia.month &&
          data.year == dia.year) {
        _selecionado = listaCardapio.first;
        entrou = true;
      }
      _dataSelecionada = dia;
    });
    if(!entrou) {
      _selecionado = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: Text("Cardápios"),
      backgroundColor: Colors.deepOrange,),
      body: _isBusy
          ? Center(child: CircularProgressIndicator())
          : Column(
              children: <Widget>[
                TableCalendar(
                  calendarController: calendarController,
                  locale: 'pt_BR',
                  events: _eventos,
                  availableCalendarFormats: const {
                    CalendarFormat.month: 'Mês',
                    CalendarFormat.twoWeeks: '2 semanas',
                    CalendarFormat.week: 'Semana',
                  },
                  calendarStyle: CalendarStyle(
                    selectedColor: Theme.of(context).primaryColor,
                    todayColor: Color(0xFFb1c3cc),
                    markersColor: Colors.black,
                  ),
                  onDaySelected: (date, events) {
                    _dataSelecionada = date;
                    _carregarEventos(date);
                    setState(() {});
                  },
                ),
                Expanded(
                    child: SingleChildScrollView(
                  child: Container(
                      padding: EdgeInsets.all(
                          MediaQuery.of(context).size.width * 0.02),
                      child: _selecionado != null
                          ? _selecionado.ativo
                              ? Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Text("Descrição: ",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16.0)),
                                        Text(_selecionado.descricao,
                                            style: TextStyle(fontSize: 15.0))
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Text("Observação: ",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16.0)),
                                        Text(_selecionado.obs,
                                            style: TextStyle(fontSize: 15.0))
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Container(
                                          width: MediaQuery.of(context).size.width*0.5,
                                            padding: EdgeInsets.only(
                                                top: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.02),
                                            child: Image.network(
                                                
                                                _selecionado.foto != null
                                                    ? _selecionado.foto
                                                    : "https://i.pinimg.com/736x/a6/93/de/a693deeb3c809a0786e98b7f19421829.jpg",
                                                scale: 5,
                                                fit:BoxFit.cover,)),
                                        Column(
                                          children: <Widget>[
                                            RaisedButton(
                                              child: Text("Editar Cardápio"),
                                              onPressed: () async {
                                                bool adicionou = await Navigator.of(context)
                                                    .push(MaterialPageRoute(
                                                        builder: (context) =>
                                                            NovoCardapioPage(
                                                                _selecionado,
                                                                _dataSelecionada,
                                                                widget
                                                                    .refeitorio,
                                                                widget.refeicao)));
                                                if (adicionou != null && adicionou) {
                                                  _scaffoldKey.currentState
                                                      .showSnackBar(SnackBar(
                                                    content: Text(
                                                        "Cardápio editado com sucesso"),
                                                  ));
                                                  _isBusy = true;
                                                  setState(() {});
                                                  _cardapios = await CardapioServ
                                                      .getCardapios(
                                                          Singleton
                                                                  .getInstance()
                                                              .instituicao
                                                              .docID,
                                                          widget
                                                              .refeitorio.docID,
                                                          widget
                                                              .refeicao.docID);
                                                  _carregarEventos(
                                                      _dataSelecionada);
                                                  _isBusy = false;
                                                  setState(() {});
                                                }
                                              },
                                            ),
                                            RaisedButton(
                                              child:
                                                  Text("Visualizar Check-ins"),
                                              onPressed: () {
                                                Navigator.of(context).push(
                                                    MaterialPageRoute(
                                                      builder: (context)=>CheckinPage(_selecionado,widget.refeitorio,widget.refeicao)
                                                    )
                                                );
                                              },
                                            ),
                                            RaisedButton(
                                    child: Text("Desabilitar Refeição"),
                                    onPressed: () async{
                                             _isBusy = true;
                                      setState(() {});
                                     _selecionado.ativo= false;
                                      await CardapioServ.editCardapio(
                                          _selecionado,
                                          Singleton.getInstance()
                                              .instituicao
                                              .docID,
                                          widget.refeitorio.docID,
                                          widget.refeicao.docID);
                                      _cardapios =
                                          await CardapioServ.getCardapios(
                                              Singleton.getInstance()
                                                  .instituicao
                                                  .docID,
                                              widget.refeitorio.docID,
                                              widget.refeicao.docID);
                                      _eventos.clear();
                                      for (Cardapio cardapio in _cardapios) {
                                      if (_eventos[cardapio.data] == null) _eventos[cardapio.data] = List();
                                        _eventos[cardapio.data].add(cardapio);
                                      }
                                      _carregarEventos(_dataSelecionada);
                                      _isBusy = false;
                                      setState(() {});


                                    },
                                  ),
                                          ],
                                        ),
                                      ],
                                    )
                                  ],
                                )
                              : Center(
                                  child: RaisedButton(
                                    child: Text("Reabilitar Refeição"),
                                    onPressed: () async{
                                             _isBusy = true;
                                      setState(() {});
                                      if(_selecionado.descricao!=null){
                                                _selecionado.ativo= true;
                                          await CardapioServ.editCardapio(
                                              _selecionado,
                                              Singleton.getInstance()
                                                  .instituicao
                                                  .docID,
                                              widget.refeitorio.docID,
                                              widget.refeicao.docID);
                                          _cardapios =
                                              await CardapioServ.getCardapios(
                                                  Singleton.getInstance()
                                                      .instituicao
                                                      .docID,
                                                  widget.refeitorio.docID,
                                                  widget.refeicao.docID);
                                          _eventos.clear();
                                          for (Cardapio cardapio in _cardapios) {
                                          if (_eventos[cardapio.data] == null) _eventos[cardapio.data] = List();
                                            _eventos[cardapio.data].add(cardapio);
                                          }
                                        
                                      }else{
                                            await CardapioServ.deleteCardapio(
                                              _selecionado,
                                              Singleton.getInstance()
                                                  .instituicao
                                                  .docID,
                                              widget.refeitorio.docID,
                                              widget.refeicao.docID);
                                          _cardapios =
                                              await CardapioServ.getCardapios(
                                                  Singleton.getInstance()
                                                      .instituicao
                                                      .docID,
                                                  widget.refeitorio.docID,
                                                  widget.refeicao.docID);
                                          _eventos.clear();
                                          for (Cardapio cardapio in _cardapios) {
                                          if (_eventos[cardapio.data] == null) _eventos[cardapio.data] = List();
                                            _eventos[cardapio.data].add(cardapio);
                                          }

                                      }
                                      _carregarEventos(_dataSelecionada);
                                          _isBusy = false;
                                      setState(() {});


                                    },
                                  ),
                                )
                          : Center(
                              child: Column(
                                children: <Widget>[
                                  RaisedButton(
                                    child: Text("Adicionar Cardápio Diário"),
                                    onPressed: () async {
                                      bool adicionou = await Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  NovoCardapioPage(
                                                      _selecionado,
                                                      _dataSelecionada,
                                                      widget.refeitorio,
                                                      widget.refeicao)));
                                      if (adicionou != null && adicionou) {
                                        _scaffoldKey.currentState
                                            .showSnackBar(SnackBar(
                                          content: Text(
                                              "Cardápio adicionado com sucesso"),
                                        ));
                                        _isBusy = true;
                                        setState(() {});
                                        _cardapios =
                                            await CardapioServ.getCardapios(
                                                Singleton.getInstance()
                                                    .instituicao
                                                    .docID,
                                                widget.refeitorio.docID,
                                                widget.refeicao.docID);
                                        _carregarEventos(_dataSelecionada);
                                        _isBusy = false;
                                        setState(() {});
                                      }
                                    },
                                  ),
                                  RaisedButton(
                                    child:
                                        Text("Desabilitar Refeição Neste Dia"),
                                    onPressed: () async {
                                      _isBusy = true;
                                      setState(() {});
                                      Cardapio cardapio = Cardapio.nulo();
                                      cardapio.data = _dataSelecionada;
                                      cardapio.ativo = false;
                                      await CardapioServ.addCardapio(
                                          cardapio,
                                          Singleton.getInstance()
                                              .instituicao
                                              .docID,
                                          widget.refeitorio.docID,
                                          widget.refeicao.docID);
                                      _cardapios =
                                          await CardapioServ.getCardapios(
                                              Singleton.getInstance()
                                                  .instituicao
                                                  .docID,
                                              widget.refeitorio.docID,
                                              widget.refeicao.docID);
                                      _eventos.clear();
                                      for (Cardapio cardapio in _cardapios) {
                                      if (_eventos[cardapio.data] == null) _eventos[cardapio.data] = List();
                                        _eventos[cardapio.data].add(cardapio);
                                      }
                                      _carregarEventos(_dataSelecionada);
                                      _isBusy = false;
                                      setState(() {});
                                    },
                                  )
                                ],
                              ),
                            )),
                )),
              ],
            ),
    );
  }
}
