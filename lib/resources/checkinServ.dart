import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ms2019_2/models/checkin.dart';

class CheckinServicos{

  static Future<String> addCheckin(String refeitorioID,String instituicaoDocID,String cardapioID, String refeicaoDocId, Checkin checkin)async{
    String retorno;
    try {
      var doc = await Firestore.instance.collection('instituicoes/$instituicaoDocID/refeitorios/$refeitorioID/refeicao/$refeicaoDocId/cardapio/$cardapioID/checkins').add(checkin.toJson());
      retorno=doc.documentID;
    } catch (e) {
      retorno=e.toString();
    }
    return retorno;
  }
}