import 'package:cloud_firestore/cloud_firestore.dart';

class Refeicao{

  String docID;

  DateTime domFim;
  DateTime domInicio;
  DateTime domMax;

  DateTime segFim;
  DateTime segInicio;
  DateTime segMax;

  DateTime terFim;
  DateTime terInicio;
  DateTime terMax;

  DateTime quaFim;
  DateTime quaInicio;
  DateTime quaMax;

  DateTime quiFim;
  DateTime quiInicio;
  DateTime quiMax;

  DateTime sexFim;
  DateTime sexInicio;
  DateTime sexMax;

  DateTime sabFim;
  DateTime sabInicio;
  DateTime sabMax;

  String tipo;


  Refeicao(
    
    this.docID,

    this.domFim,
    this.domInicio,
    this.domMax,

    this.segFim,
    this.segInicio,
    this.segMax,

    this.terFim,
    this.terInicio,
    this.terMax,

    this.quaFim,
    this.quaInicio,
    this.quaMax,

    this.quiFim,
    this.quiInicio,
    this.quiMax,

    this.sexFim,
    this.sexInicio,
    this.sexMax,

    this.sabFim,
    this.sabInicio,
    this.sabMax,

    this.tipo
  );

  Refeicao.nulo() {
    docID = domFim = domInicio = domMax = segMax = segInicio = segFim = terMax = 
    terInicio  = terFim = quaMax = quaInicio = quaFim = quiMax = quiFim = quiInicio = 
    sexMax = sexInicio = sexFim = sabMax = sabInicio = sabFim = tipo = null;
  }

  Refeicao.fromJson(Map<String, dynamic> dados, String _docID) {

    docID = _docID;

    domInicio = dados['domInicio'] != null ? dados['domInicio'].toDate() : null;
    domMax = dados['domMax'] != null ? dados['domMax'].toDate() : null;
    domFim = dados['domFim'] != null ? dados['domFim'].toDate() : null;

    segInicio = dados['segInicio'] != null ? dados['segInicio'].toDate() : null;
    segFim = dados['segFim'] != null ? dados['segFim'].toDate() : null;
    segMax = dados['segMax'] != null ? dados['segMax'].toDate() : null;

    terInicio = dados['terInicio'] != null ? dados['terInicio'].toDate() : null;
    terFim = dados['terFim'] != null ? dados['terFim'].toDate() : null;
    terMax = dados['terMax'] != null ? dados['terMax'].toDate() : null;

    quaInicio = dados['quaInicio'] != null ? dados['quaInicio'].toDate() : null;
    quaFim = dados['quaFim'] != null ? dados['quaFim'].toDate() : null;
    quaMax = dados['quaMax'] != null ? dados['quaMax'].toDate() : null;

    quiInicio = dados['quiInicio'] != null ? dados['quiInicio'].toDate() : null;
    quiFim = dados['quiFim'] != null ? dados['quiFim'].toDate() : null;
    quiMax = dados['quiMax'] != null ? dados['quiMax'].toDate() : null;

    sexInicio = dados['sexInicio'] != null ? dados['sexInicio'].toDate() : null;
    sexFim = dados['sexFim'] != null ? dados['sexFim'].toDate() : null;
    sexMax = dados['sexMax'] != null ? dados['sexMax'].toDate() : null;

    sabInicio = dados['sabInicio'] != null ? dados['sabInicio'].toDate() : null;
    sabMax = dados['sabMax'] != null ? dados['sabMax'].toDate() : null;
    sabFim = dados['sabFim'] != null ? dados['sabFim'].toDate() : null;

    tipo = dados['tipo'];
  }

  Map<String, dynamic> toJson() => {

    "docID": docID,

    "domInicio": domInicio != null ? Timestamp.fromDate(domInicio) : null,
    "domMax": domMax != null ? Timestamp.fromDate(domMax) : null,
    "domFim": domFim != null ? Timestamp.fromDate(domFim) : null,

    "segInicio": segInicio != null ? Timestamp.fromDate(segInicio) : null,
    "segMax": segMax != null ? Timestamp.fromDate(segMax) : null,
    "segFim": segFim != null ? Timestamp.fromDate(segFim) : null,

    "terInicio": terInicio != null ? Timestamp.fromDate(terInicio) : null,
    "terMax": terMax != null ? Timestamp.fromDate(terMax) : null,
    "terFim": terFim != null ? Timestamp.fromDate(terFim) : null,

    "quaInicio": quaInicio != null ? Timestamp.fromDate(quaInicio) : null,
    "quaMax": quaMax != null ? Timestamp.fromDate(quaMax) : null,
    "quaFim": quaFim != null ? Timestamp.fromDate(quaFim) : null,

    "quiInicio": quiInicio != null ? Timestamp.fromDate(quiInicio) : null,
    "quiMax": quiMax != null ? Timestamp.fromDate(quiMax) : null,
    "quiFim": quiFim != null ? Timestamp.fromDate(quiFim) : null,

    "sexInicio": sexInicio != null ? Timestamp.fromDate(sexInicio) : null,
    "sexMax": sexMax != null ? Timestamp.fromDate(sexMax) : null,
    "sexFim": sexFim != null ? Timestamp.fromDate(sexFim) : null,

    "sabInicio": sabInicio != null ? Timestamp.fromDate(sabInicio) : null,
    "sabMax": sabMax != null ? Timestamp.fromDate(sabMax) : null,
    "sabFim": sabFim != null ? Timestamp.fromDate(sabFim) : null,

    "tipo": tipo
  };
}