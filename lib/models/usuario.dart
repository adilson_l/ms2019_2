class Usuario {
    String docID;
    String cpf;
    String email;
    String nome;
    List<String> vinculos;


    Usuario(
        this.cpf,
        this.email,
        this.nome,
        this.vinculos,
    );


    Usuario.fromJson(Map<String, dynamic> dados, String _docID)
    {
        docID = _docID;
        cpf=dados["cpf"];
        email=dados["email"];
        nome=dados["nome"];
        vinculos=List<String>.from(dados["vinculos"].map((x) => x));
    }

    Map<String, dynamic> toJson() => {
        "cpf": cpf,
        "email": email,
        "nome": nome,
        "vinculos": List<dynamic>.from(vinculos.map((x) => x)),
    };
}