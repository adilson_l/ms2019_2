import 'dart:io';

import 'package:date_format/date_format.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:ms2019_2/models/cardapio.dart';
import 'package:ms2019_2/models/refeicao.dart';
import 'package:ms2019_2/models/refeitorio.dart';
import 'package:ms2019_2/resources/cardapioServ.dart';
import 'package:ms2019_2/resources/singleton.dart';

class NovoCardapioPage extends StatefulWidget {
  final Cardapio cardapio;
  final DateTime data;
  final Refeitorio refeitorio;
  final Refeicao refeicao;

  NovoCardapioPage(this.cardapio, this.data, this.refeitorio, this.refeicao);

  @override
  _NovoCardapioPageState createState() => _NovoCardapioPageState();
}

class _NovoCardapioPageState extends State<NovoCardapioPage> {
  TextEditingController _descricao = TextEditingController();
  TextEditingController _obs = TextEditingController();
  Cardapio _cardapio = Cardapio.nulo();
    final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  File _image;
  String imagem;
  @override
  void initState() {
    if(widget.cardapio != null) {
      _descricao.text = widget.cardapio.descricao;
      _obs.text = widget.cardapio.obs;
    }
    _cardapio.data = widget.data;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.deepOrange,
        title: Text("Editar Cardápio " + formatDate(widget.data, [dd, "/", mm, "/", yyyy])),
      ),
      body: Container(
        padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
        child: Column(
          children: <Widget>[
            TextField(
              controller: _descricao,
              decoration: InputDecoration(
                labelText: "Descrição",
                hintText: "Ex.: Arroz, feijão, carne moída..."
              ),
            ),
            TextField(
              controller: _obs,
              decoration: InputDecoration(
                labelText: "Observação",
                hintText: "Ex.: Almoço em comemoração do dia das mães..."
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8),
              child: Center(
                          child: Text("Foto do prato"),
            ),
            ),
            
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
                RaisedButton(
              child: Text("Foto"),
              onPressed: () async{
                 var image = await ImagePicker.pickImage( source: ImageSource.camera);
                 if(image != null) {
                      _image = image;
                       StorageUploadTask task = FirebaseStorage.instance.ref().child('FotosCardapios/' +
                                                Singleton.getInstance().instituicao.nome+'/'+DateTime.now().toString())
                                            .putFile(_image);
                   task.onComplete.then((value) {
                                          value.ref.getDownloadURL().then((url) {
                                              imagem=url.toString();
                                          });
                                        });
                 }else{
                   _scaffoldKey.currentState.showSnackBar(SnackBar(
                     content: Text('Erro'),
                   ));
                 }
                   
              },
            ),
             RaisedButton(
              child: Text("Galeria"),
              onPressed: () async{
                 var image = await ImagePicker.pickImage( source: ImageSource.gallery);
                 if(image != null) {
                      _image = image;
                       StorageUploadTask task = FirebaseStorage.instance.ref().child('FotosCardapios/' +
                                                Singleton.getInstance().instituicao.nome+'/'+DateTime.now().toString())
                                            .putFile(_image);
                   task.onComplete.then((value) {
                                          value.ref.getDownloadURL().then((url) {
                                              imagem=url.toString();
                                          });
                                        });
                 }else{
                   _scaffoldKey.currentState.showSnackBar(SnackBar(
                     content: Text('Erro'),
                   ));
                 }
                   
              },
            )
            ],
          ),
            Container(
              width: MediaQuery.of(context).size.width * 0.9,
              child: RaisedButton(
                child: Text("Salvar"),
                onPressed: () async {
                   if(widget.cardapio == null) {
                  _cardapio.ativo = true;
                  _cardapio.descricao = _descricao.text;
                  _cardapio.foto= imagem==null? null : imagem;
                  _cardapio.obs = _obs.text;
                    await CardapioServ.addCardapio(_cardapio, Singleton.getInstance().instituicao.docID, widget.refeitorio.docID, widget.refeicao.docID);
                  } else {
                    widget.cardapio.descricao=_descricao.text;
                    widget.cardapio.obs=_obs.text;
                    widget.cardapio.foto=imagem==null? null : imagem;
                    await CardapioServ.editCardapio(widget.cardapio, Singleton.getInstance().instituicao.docID, widget.refeitorio.docID, widget.refeicao.docID);
                  }
                  Navigator.of(context).pop(true);
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}