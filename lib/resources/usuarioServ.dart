import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ms2019_2/models/usuario.dart';
import 'package:ms2019_2/models/vinculo.dart';

class UsuarioServ {
  static Future<String> addUsuario(Usuario usuario) async {
    String retorno;
    try {
      var doc =
          await Firestore.instance.collection('usuarios').add(usuario.toJson());
      retorno = doc.documentID;
    } catch (e) {
      retorno = e.toString();
    }
    return retorno;
  }

  static Future<bool> editUsuario(Usuario usuario) async {
    bool retorno;
    try {
      await Firestore.instance
          .collection('usuarios')
          .document(usuario.docID)
          .updateData(usuario.toJson());
      retorno = true;
    } catch (e) {
      print(e.toString());
      retorno = false;
    }
    return retorno;
  }

  static Future<Usuario> getUsuario(String email) async {
    Usuario usuario;

    try {
      QuerySnapshot querySnapshot = await Firestore.instance
          .collection('usuarios')
          .where('email', isEqualTo: email)
          .getDocuments();

      if (querySnapshot.documents.isEmpty) {
        return null;
      } else {
        usuario = Usuario.fromJson(querySnapshot.documents.first.data,
            querySnapshot.documents.first.documentID);

        return usuario;
      }
    } catch (e) {
      print(e.toString());

      return null;
    }
  }

  static Future<List<Vinculo>> getVinculos(List<String> codigos) async {
    try {
      QuerySnapshot res = await Firestore.instance
          .collection('vinculos')
          .getDocuments();
      List<Vinculo> vinculos = List();
      for (var doc in res.documents) {
        Vinculo vinculo = Vinculo.fromJson(doc.data, doc.documentID);
        vinculos.add(vinculo);
      }
      return vinculos.where((v) => codigos.contains(v.codigo)).toList();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
