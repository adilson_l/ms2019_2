import 'package:flutter/material.dart';
import 'package:ms2019_2/views/pages/login/cadastroUsuPage.dart';
import 'package:ms2019_2/resources/loginServ.dart';
import 'package:ms2019_2/views/pages/usuario/main/usuarioMainPage.dart';

class UsuarioLoginPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey;

  UsuarioLoginPage(this._scaffoldKey);

  @override
  _UsuarioLoginPageState createState() => _UsuarioLoginPageState();
}

class _UsuarioLoginPageState extends State<UsuarioLoginPage> {
  bool _hidePassword;
  TextEditingController _emailController, _senhaController;
  FocusNode _emailFocus, _senhaFocus;
  GlobalKey<FormState> _formKey;

  @override
  void initState() {
    _hidePassword = true;
    _emailController = TextEditingController();
    _senhaController = TextEditingController();
    _emailFocus = FocusNode();
    _senhaFocus = FocusNode();
    _formKey = GlobalKey<FormState>();
    super.initState();
  }

  void autenticar() async {
    if(_formKey.currentState.validate()) {
      showDialog(
        context: context,
        builder: (context) => Container(
        color: Colors.transparent,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      )
      );
     bool logou= await LoginServ.logInWithEmailUsuario(context, _emailController.text, _senhaController.text, widget._scaffoldKey);
     if(logou){
        Navigator.of(context).pop();
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => UsuarioMainPage()),
            (Route<dynamic> route) => false);
     }else{
             Navigator.of(context).pop();
     }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.05),
                child: Image.asset("img/logo.png")),
              Text('Usuário',
                  style: TextStyle(color: Colors.white, fontSize: 24)),
              Padding(
                padding: EdgeInsetsDirectional.only(
                    bottom: MediaQuery.of(context).size.height * 0.03),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width * 0.05),
                child: TextFormField(
                  controller: _emailController,
                  focusNode: _emailFocus,
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                      labelText: 'Email',
                      labelStyle: TextStyle(color: Colors.white),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white)),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                      )),
                  validator: (String valor) {
                    Pattern pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regex = new RegExp(pattern);
                    if (!regex.hasMatch(valor))
                      return 'Digite um e-mail válido';
                    else
                      return null;
                  },
                  onFieldSubmitted: (valor) {
                    FocusScope.of(context).unfocus();
                    FocusScope.of(context).requestFocus(_senhaFocus);
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height * 0.01),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width * 0.05),
                child: TextFormField(
                  controller: _senhaController,
                  focusNode: _senhaFocus,
                  style: TextStyle(color: Colors.white),
                  obscureText: _hidePassword,
                  decoration: InputDecoration(
                      labelText: 'Senha',
                      labelStyle: TextStyle(color: Colors.white),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white)),
                          suffixIcon: IconButton(
                      icon: _hidePassword
                          ? Icon(Icons.visibility_off, color: Colors.white)
                          : Icon(Icons.visibility, color: Colors.white),
                      onPressed: () {
                        _hidePassword = !_hidePassword;
                        setState(() {});
                      },
                    ),),
                  validator: (valor) {
                    if (valor.isEmpty) return "Informe a senha";
                    return null;
                  },
                  onEditingComplete: autenticar,
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.02),
                width: MediaQuery.of(context).size.width * 0.9,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(15.0)),
                  color: Colors.blueAccent,
                  child: Text(
                    "Entrar",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: autenticar,
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.9,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(15.0)),
                  color: Colors.white,
                  child: Text(
                    "Cadastrar",
                    style: TextStyle(color: Colors.blueAccent),
                  ),
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => CadastroUsuarioPage()));
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
