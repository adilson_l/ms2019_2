import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ms2019_2/models/refeicao.dart';
import 'package:ms2019_2/models/refeitorio.dart';
import 'package:ms2019_2/resources/refeicaoServ.dart';
import 'package:ms2019_2/resources/singleton.dart';
import 'package:ms2019_2/views/pages/instituicao/refeicoes/cardapiosPage.dart';
import 'package:ms2019_2/views/pages/instituicao/refeicoes/novaRefeicaoPage.dart';
import 'package:ms2019_2/views/pages/instituicao/refeicoes/refeicoesPage.dart';
import 'package:qr_flutter/qr_flutter.dart';

class MainRefeitorios extends StatefulWidget {
  final Refeitorio refeitorio;

  MainRefeitorios(this.refeitorio);

  @override
  _MainRefeitoriosState createState() => _MainRefeitoriosState();
}

class _MainRefeitoriosState extends State<MainRefeitorios> {
  bool _isBusy;
  List<Refeicao> refeicoes;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    _isBusy = true;
    RefeicaoServ.getRefeicoes(Singleton.getInstance().instituicao.docID, widget.refeitorio.docID).then((refeicoes) {
      this.refeicoes = refeicoes;
      _isBusy = false;
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(widget.refeitorio.descricao),
        backgroundColor: Colors.deepOrange,
        actions: <Widget>[
          IconButton(
            icon: Icon(FontAwesomeIcons.qrcode),
            onPressed: () {
              showDialog(
                    context: context,
                    builder: (context)=>Dialog(
                      child:Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          QrImage(
                      data: widget.refeitorio.codigoQR,
                      version: QrVersions.auto,
                      size: 320,
                      gapless: false,
                    ),
                    Text(widget.refeitorio.codigoQR)
                        ],
                      ) 
                    )
                  );
            },
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Colors.deepOrange,
        onPressed: () async {
          bool retorno = await Navigator.push(context, MaterialPageRoute(builder: (context) => NovaRefeicaoPage(widget.refeitorio, null)));
          if(retorno != null) {
            if(retorno) {
              _scaffoldKey.currentState.showSnackBar(SnackBar(
                content: Text("Alterações realizadas com sucesso"),
              ));
            } else {
              _scaffoldKey.currentState.showSnackBar(SnackBar(
                content: Text("Falha na requisição"),
              ));
            }
            _isBusy = true;
            RefeicaoServ.getRefeicoes(Singleton.getInstance().instituicao.docID, widget.refeitorio.docID).then((refeicoes) {
              this.refeicoes = refeicoes;
              _isBusy = false;
              setState(() {});
            });
          }
        },
      ),
      body: _isBusy ? Center(child: CircularProgressIndicator()) : Container(
        padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
        child: ListView.builder(
          itemCount: refeicoes.length,
          itemBuilder: (context, i) => Card(
                      child: Container(
                        padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
                        height: MediaQuery.of(context).size.height * 0.1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(refeicoes[i].tipo, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0)),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                IconButton(
                                  icon: Icon(FontAwesomeIcons.calendar, color: Colors.red),
                                  onPressed: () {
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => CardapiosPage(widget.refeitorio, refeicoes[i])));
                                  },
                                ),
                                IconButton(
                                  icon: Icon(FontAwesomeIcons.pencilAlt, color: Colors.green,),
                                  onPressed: () async {
                                    bool retorno = await Navigator.push(context, MaterialPageRoute(builder: (context) => NovaRefeicaoPage(widget.refeitorio, refeicoes[i])));
                                    if(retorno != null) {
                                      if(retorno) {
                                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                                          content: Text("Alterações realizadas com sucesso"),
                                        ));
                                      } else {
                                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                                          content: Text("Falha na requisição"),
                                        ));
                                      }
                                      _isBusy = true;
                                      RefeicaoServ.getRefeicoes(Singleton.getInstance().instituicao.docID, widget.refeitorio.docID).then((refeicoes) {
                                        this.refeicoes = refeicoes;
                                        _isBusy = false;
                                        setState(() {});
                                      });
                                    }
                                  },
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    )
        ),
        // child: Column(
        //   children: <Widget>[
        //     Container(
        //       width: MediaQuery.of(context).size.width,
        //       child: RaisedButton(
        //         color: Theme.of(context).primaryColorDark,
        //         child: Text("Gerenciar Cardápios", style: TextStyle(color: Colors.white)),
        //         onPressed: () {
        //           Navigator.of(context).push(MaterialPageRoute(builder: (context) => CardapiosPage(widget.refeitorio)));
        //         },
        //       ),
        //     ),
        //     Container(
        //       width: MediaQuery.of(context).size.width,
        //       child: RaisedButton(
        //         color: Theme.of(context).primaryColorDark,
        //         child: Text("Gerenciar Horários", style: TextStyle(color: Colors.white)),
        //         onPressed: () {
        //           Navigator.of(context).push(MaterialPageRoute(builder: (context) => RefeicoesPage(widget.refeitorio)));
        //         },
        //       ),
        //     ),
        //     Container(
        //       width: MediaQuery.of(context).size.width,
        //       child: RaisedButton(
        //         color: Theme.of(context).primaryColorDark,
        //         child: Text("Ver código/QR Code", style: TextStyle(color: Colors.white)),
        //         onPressed: () {
        //           showDialog(
        //             context: context,
        //             builder: (context)=>Dialog(
        //               child:Column(
        //                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //                 children: <Widget>[
        //                   QrImage(
        //               data: Singleton.getInstance().instituicao.docID,
        //               version: QrVersions.auto,
        //               size: 320,
        //               gapless: false,
        //             ),
        //             Text(Singleton.getInstance().instituicao.docID)
        //                 ],
        //               ) 
        //             )
        //           );
        //         },
        //       ),
        //     )
        //   ],
        // ),
      ),
    );
  }
}