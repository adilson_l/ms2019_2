import 'package:flutter/material.dart';


class FundoGradienteHot extends StatefulWidget {
  @override
  _FundoGradienteHotState createState() => _FundoGradienteHotState();
}

class _FundoGradienteHotState extends State<FundoGradienteHot> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.orange,Colors.orange[200],Colors.red[200],Colors.red],
                begin: Alignment.bottomLeft,
                end: Alignment.topRight,
              )
            ),
    );
  }
}