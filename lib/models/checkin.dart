class Checkin{

  bool  checkin;
  String usuarioId;
  String usuarioNome;
  String docId;

  Checkin(this.checkin,this.usuarioId,this.usuarioNome,{this.docId});

  Checkin.nulo(){
    checkin=usuarioNome=usuarioId=docId=null;
  }

  Checkin.fromJson(Map<String,dynamic> dados, String _docId){
    checkin=dados['checkin'];
    usuarioId=dados['usuarioId'];
    usuarioNome=dados["usuarioNome"];
    docId=_docId;
  }

  Map<String,dynamic> toJson(){
    
    return <String,dynamic>{
            'checkin':checkin,
            'usuarioNome':usuarioNome,
            'usuarioId':usuarioId
    };
}


}