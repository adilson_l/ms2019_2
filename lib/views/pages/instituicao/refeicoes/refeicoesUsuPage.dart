import 'package:flutter/material.dart';
import 'package:ms2019_2/models/refeicao.dart';
import 'package:ms2019_2/models/refeitorio.dart';
import 'package:ms2019_2/resources/refeicaoServ.dart';
import 'package:ms2019_2/views/pages/instituicao/refeicoes/cardapiosUsuPage.dart';

class RefeicoesUsuPage extends StatefulWidget {
  final Refeitorio refeitorio;
  final String instituicaoDocId;

  RefeicoesUsuPage(this.refeitorio, this.instituicaoDocId);

  @override
  _RefeicoesUsuPageState createState() => _RefeicoesUsuPageState();
}

class _RefeicoesUsuPageState extends State<RefeicoesUsuPage> {
  bool _isBusy;
  List<Refeicao> refeicoes;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    _isBusy = true;
    RefeicaoServ.getRefeicoes(widget.instituicaoDocId, widget.refeitorio.docID)
        .then((refeicoes) {
      this.refeicoes = refeicoes;
      _isBusy = false;
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(widget.refeitorio.descricao),
        ),
        body: _isBusy
            ? Center(child: CircularProgressIndicator())
            : Container(
                padding:
                    EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
                child: ListView.builder(
                  itemCount: refeicoes.length,
                  itemBuilder: (context, i) => GestureDetector(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => CardapiosPageUsu(widget.refeitorio, widget.instituicaoDocId, refeicoes[i])));
                    },
                    child: Card(
                      child: Container(
                        padding: EdgeInsets.all(
                            MediaQuery.of(context).size.width * 0.02),
                        height: MediaQuery.of(context).size.height * 0.1,
                        child: Center(
                            child: Text(refeicoes[i].tipo,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16.0))),
                      ),
                    ),
                  ),
                )));
  }
}
