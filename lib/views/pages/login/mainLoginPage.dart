import 'package:flutter/material.dart';
import 'package:ms2019_2/views/components/fundoGradienteCold.dart';
import 'package:ms2019_2/views/components/fundoGradienteHot.dart';
import 'package:ms2019_2/views/pages/login/instituicaoLoginPage.dart';
import 'package:ms2019_2/views/pages/login/usuarioLoginPage.dart';

class MainLoginPage extends StatefulWidget {
  @override
  _MainLoginPageState createState() => _MainLoginPageState();
}

class _MainLoginPageState extends State<MainLoginPage> with SingleTickerProviderStateMixin {
  TabController _tabController;
  GlobalKey<ScaffoldState> _scaffoldKey;

  @override
  void initState() {
    _scaffoldKey = GlobalKey<ScaffoldState>();
    _tabController = TabController(vsync: this, length: 2);
    super.initState();
    _tabController.addListener(() {
      FocusScope.of(context).unfocus();
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: <Widget>[
          _tabController.index == 0 ? FundoGradienteCold() : FundoGradienteHot(),
          Scaffold(
            key: _scaffoldKey,
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              bottom: TabBar(
                  isScrollable: true,
                  controller: _tabController,
                  unselectedLabelColor: Colors.white,
                  indicatorSize: TabBarIndicatorSize.label,
                  indicatorColor: Colors.white,
                  indicator: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white
                  ),
                  tabs: [
                    Tab(
                      child: Icon(
                        Icons.panorama_fish_eye,
                      )
                    ),
                    Tab(
                      child: Icon(
                        Icons.panorama_fish_eye,
                      )
                    ),
                  ]),
            ),
            body: Stack(
              children: <Widget>[
                TabBarView(
                  controller: _tabController,
                  children: [
                  UsuarioLoginPage(_scaffoldKey),
                  InstituicaoLoginPage(_scaffoldKey)
                ]),
              ],
            ),
          ),
        ],
      )
    );
  }
}