import 'package:flutter/material.dart';
import 'package:ms2019_2/models/refeicao.dart';
import 'package:ms2019_2/models/refeitorio.dart';
import 'package:ms2019_2/resources/refeicaoServ.dart';
import 'package:ms2019_2/resources/singleton.dart';

class RefeicoesPage extends StatefulWidget {
  final Refeitorio refeitorio;

  RefeicoesPage(this.refeitorio);

  @override
  _RefeicoesPageState createState() => _RefeicoesPageState();
}

class _RefeicoesPageState extends State<RefeicoesPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  List<String> semana = ["Segunda-Feira", "Terça-Feira", "Quarta-Feira", "Quinta-Feira", "Sexta-Feira", "Sábado", "Domingo"];
  TextEditingController _inicio = TextEditingController();
  TextEditingController _fim = TextEditingController();
  TextEditingController _max = TextEditingController();
  Refeicao _refeicao;
  bool _isBusy;

  @override
  void initState() {
    _isBusy = true;
    // RefeicaoServ.getRefeicao(Singleton.getInstance().instituicao.docID, widget.refeitorio.docID).then((refeicao) {
    //   _refeicao = refeicao != null ? refeicao : Refeicao.nulo();
    //   _isBusy = false;
    //   setState(() {});
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Refeições"),
      ),
      body: _isBusy ? Center(child: CircularProgressIndicator()) : Container(
        padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
        child: ListView.builder(
          itemCount: semana.length,
          itemBuilder: (context, i) => Container(
              width: MediaQuery.of(context).size.width,
              child: RaisedButton(
                color: Theme.of(context).primaryColorDark,
                child: Text(semana[i], style: TextStyle(color: Colors.white)),
                onPressed: () async {
                  await showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                      content: SingleChildScrollView(
                        child: Column(
                          children: <Widget>[
                            TextField(
                              controller: _inicio,
                              maxLength: 5,
                              decoration: InputDecoration(
                                labelText: "Hora de iníco",
                              ),
                            ),
                            TextField(
                              controller: _fim,
                              maxLength: 5,
                              decoration: InputDecoration(
                                labelText: "Hora de fim",
                              ),
                            ),
                            TextField(
                              controller: _max,
                              maxLength: 5,
                              decoration: InputDecoration(
                                labelText: "Hora máxima de check-in",
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.5,
                              child: RaisedButton(
                                child: Text("Salvar"),
                                onPressed: () async {
                                  DateTime inicio = DateTime(2000, 01, 01, int.parse(_inicio.text.split(":").first), int.parse(_inicio.text.split(":").last));
                                  DateTime fim = DateTime(2000, 01, 01, int.parse(_fim.text.split(":").first), int.parse(_fim.text.split(":").last));
                                  DateTime max = DateTime(2000, 01, 01, int.parse(_max.text.split(":").first), int.parse(_max.text.split(":").last));
                                  Navigator.of(context).pop();
                                  _isBusy = true;
                                  setState(() {});
                                  if(await RefeicaoServ.editDiaSemana(Singleton.getInstance().instituicao.docID, widget.refeitorio.docID, _refeicao.docID, i, inicio, fim, max)) {
                                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                                      content: Text("Dia da semana editado com sucesso"),
                                    ));
                                  } else {
                                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                                      content: Text("Ocorreu algum problema"),
                                    ));
                                  }
                                  _isBusy = false;
                                  setState(() {});
                                },
                              ),
                            )
                          ],
                        )
                      ),
                    )
                  );
                },
              ),
            ),
        )
      ),
    );
  }
}