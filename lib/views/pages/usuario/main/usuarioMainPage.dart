import 'package:fast_qr_reader_view/fast_qr_reader_view.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ms2019_2/models/refeitorio.dart';
import 'package:ms2019_2/models/vinculo.dart';
import 'package:ms2019_2/resources/askForPermission.dart';
import 'package:ms2019_2/resources/refeitorioServ.dart';
import 'package:ms2019_2/resources/singleton.dart';
import 'package:ms2019_2/resources/usuarioServ.dart';
import 'package:ms2019_2/views/pages/instituicao/refeicoes/refeicoesUsuPage.dart';
import 'package:ms2019_2/views/pages/login/mainLoginPage.dart';
import 'package:ms2019_2/views/pages/usuario/main/lerQrcode.dart';
import 'package:unicorndial/unicorndial.dart';

class UsuarioMainPage extends StatefulWidget {
  @override
  _UsuarioMainPageState createState() => _UsuarioMainPageState();
}

class _UsuarioMainPageState extends State<UsuarioMainPage> {
  bool _isBusy;
  List<Vinculo> _vinculos;
  TextEditingController dialogController = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    _carregarVinculos();
    super.initState();
  }

  _carregarVinculos() {
    _isBusy = true;
    UsuarioServ.getVinculos(Singleton.getInstance().usuario.vinculos)
        .then((vinculos) {
      _vinculos = vinculos;
      _isBusy = false;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    var childButtons = List<UnicornButton>();

    childButtons.add(UnicornButton(
        hasLabel: true,
        labelText: "Ler QrCode",
        currentButton: FloatingActionButton(
          heroTag: "camera",
          backgroundColor: Colors.redAccent,
          mini: true,
          child: Icon(Icons.camera_alt),
          onPressed: () async {
            List<CameraDescription> cameras;
            cameras = await availableCameras();
            await Navigator.push(context, MaterialPageRoute(builder: (context) => AskForPermission()));
            int res = await Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => LerQrCode(cameras)));
            if(res != null) {
              switch (res) {
                case 0:
                  _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Instituição já vinculada")));
                  break;
                case 1:
                  _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Instituição vinculada com sucesso")));
                  _carregarVinculos();
                  setState(() {});
                  break;
                case 2:
                  _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Código inválido")));
                  break;
              }
            }
          },
        )));

    childButtons.add(UnicornButton(
        hasLabel: true,
        labelText: "Digitar código",
        currentButton: FloatingActionButton(
            heroTag: "edit",
            backgroundColor: Colors.greenAccent,
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) => Dialog(
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.3,
                          child: Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                                child: Text(
                                  'Digite o código:',
                                  style: TextStyle(fontSize: 16),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 16),
                                child: TextFormField(
                                  controller: dialogController,
                                ),
                              ),
                              RaisedButton(
                                child: Text('Vincular'),
                                onPressed: () async {
                                  _isBusy = true;
                                  Navigator.of(context).pop();
                                  List<Vinculo> vinculos = await UsuarioServ.getVinculos([dialogController.text]);
                                  if(Singleton.getInstance().usuario.vinculos.contains(dialogController.text)) {
                                    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Instituição já vinculada")));
                                  } else {
                                    if(vinculos.isNotEmpty) {
                                      Singleton.getInstance().usuario.vinculos.add(dialogController.text);
                                      await UsuarioServ.editUsuario(Singleton.getInstance().usuario);
                                      _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Instituição vinculada com sucesso")));
                                      _carregarVinculos();
                                    } else {
                                      _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Código inválido")));
                                    }
                                  }
                                  dialogController.clear();
                                  _isBusy = false;
                                  setState(() {});
                                },
                              )
                            ],
                          ),
                        ),
                      ));
            },
            mini: true,
            child: Icon(Icons.edit))));
    return Scaffold(
      key: _scaffoldKey,
      floatingActionButton: UnicornDialer(
          backgroundColor: Color.fromRGBO(255, 255, 255, 0.6),
          parentButtonBackground: Colors.redAccent,
          orientation: UnicornOrientation.VERTICAL,
          parentButton: Icon(Icons.add),
          childButtons: childButtons),
      appBar: AppBar(
        title: Text('Usuário'),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () async {
              await FirebaseAuth.instance.signOut();
              Singleton.getInstance().instituicao = null;
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => MainLoginPage()),
                  (Route<dynamic> route) => false);
            },
          )
        ],
      ),
      body: _isBusy
          ? Center(child: CircularProgressIndicator())
          : GridView.count(
              crossAxisCount: 2,
              children: construirGrid(),
            ),
    );
  }

  List<Widget> construirGrid() {
    List<Widget> retorno = [];
    _vinculos.forEach((f) {
      retorno.add(GestureDetector(
        child: Container(
          child: Card(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(f.instituicaoNome, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17.0)),
                Container(height: MediaQuery.of(context).size.height * 0.02),
                Text(f.refeitorioNome, style: TextStyle(fontSize: 15.0)),
                Container(height: MediaQuery.of(context).size.height * 0.02),
                Text(f.codigo, style: TextStyle(fontSize: 15.0))
              ],
            ),
          ),
        ),
        onTap: () async {
           Refeitorio refeitorio = await RefeitorioServ.getRefeitorio(f.instituicao, f.refeitorio);
              Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context)=>RefeicoesUsuPage(refeitorio, f.instituicao)
                  )
              );
        },
      ));
    });
    if (retorno.isEmpty) retorno.add(Text('Não há empresas cadastradas'));
    return retorno;
  }
}
