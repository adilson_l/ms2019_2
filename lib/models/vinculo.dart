class Vinculo {
  String docID;
  String codigo;
  String instituicao;
  String instituicaoNome;
  String refeitorio;
  String refeitorioNome;

  Vinculo({this.docID, this.codigo, this.instituicao, this.instituicaoNome, this.refeitorio, this.refeitorioNome});

  Vinculo.fromJson(Map<String, dynamic> dados, String _docID) {
      docID = _docID;
      codigo=dados["codigo"];
      instituicao=dados["instituicao"];
      instituicaoNome=dados["instituicaoNome"];
      refeitorio=dados["refeitorio"];
      refeitorioNome=dados["refeitorioNome"];
  }

  Map<String, dynamic> toJson() => {
      "codigo": codigo,
      "instituicao": instituicao,
      "instituicaoNome": instituicaoNome,
      "refeitorio": refeitorio,
      "refeitorioNome": refeitorioNome
  };
}