import 'package:flutter/material.dart';
import 'package:ms2019_2/models/refeitorio.dart';
import 'package:ms2019_2/resources/refeitorioServ.dart';
import 'package:ms2019_2/resources/singleton.dart';

class CadastroRefeitorioPage extends StatefulWidget {
  @override
  _CadastroRefeitorioPageState createState() => _CadastroRefeitorioPageState();
}

class _CadastroRefeitorioPageState extends State<CadastroRefeitorioPage> {
  TextEditingController descricaoController = TextEditingController();
  TextEditingController localizacaoController = TextEditingController();
  GlobalKey<FormState> _key = GlobalKey();
  GlobalKey<ScaffoldState> _scafoldkey=GlobalKey();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scafoldkey,
      appBar: AppBar(
        backgroundColor: Colors.deepOrange,
        title: Text('Cadastro Refeitorio'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _key,
          child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height * 0.01),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.05),
              child: TextFormField(
                controller: localizacaoController,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Campo inválido';
                  } else {
                    return null;
                  }
                },
                decoration: InputDecoration(
                    labelText: 'Localização',
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.deepOrange)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.deepOrange))),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height * 0.01),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.05),
              child: TextFormField(
                controller: descricaoController,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Campo inválido';
                  } else {
                    return null;
                  }
                },
                maxLines: 4,
                decoration: InputDecoration(
                    labelText: 'Descrição',
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.deepOrange)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.deepOrange))),
              ),
            ),
                Padding(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height * 0.01),
            ),
            RaisedButton(
              child: Text('Cadastrar',style: TextStyle(color: Colors.white),),
              color: Colors.deepOrange,
              onPressed: ()async{
                if(_key.currentState.validate()){
                  Refeitorio refeitorio= Refeitorio(descricaoController.text, localizacaoController.text);
                  await  RefeitorioServ.addRefeitorio(refeitorio, Singleton.getInstance().instituicao);
                  _scafoldkey.currentState.showSnackBar(SnackBar(
                    content: Text('Inserido com sucesso'),
                  ));
                  Navigator.of(context).pop();
                }else{
                   _scafoldkey.currentState.showSnackBar(SnackBar(
                    content: Text('Revise os campos'),
                  ));
                }
              },
            )

          ],
        ),
        )
      ),
    );
  }
}
