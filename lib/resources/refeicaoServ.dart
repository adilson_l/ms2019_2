import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ms2019_2/models/refeicao.dart';

class RefeicaoServ{

  static Future<String> addRefeicao(Refeicao refeicao,String instituicaoDocID,String refeitorioDocID)async{
    String retorno;
    try {
      var doc = await Firestore.instance.collection('instituicoes/$instituicaoDocID/refeitorios/$refeitorioDocID/refeicao').add(refeicao.toJson());
      retorno=doc.documentID;
    } catch (e) {
      retorno=e.toString();
    }
    return retorno;
  }

   static Future<bool> editRefeicao(Refeicao refeicao,String instituicaoDocID, String refeitorioDocID)async{
    bool retorno;
    try {
      await Firestore.instance.collection('instituicoes/$instituicaoDocID/refeitorios/$refeitorioDocID/refeicao').document(refeicao.docID).updateData(refeicao.toJson());
      retorno=true;
    } catch (e) {
      print(e.toString());
      retorno=false;
    }
    return retorno;
  }

  static Future<List<Refeicao>> getRefeicoes(String instituicaoDocID, String refeitorioDocID) async {
    try {
      List<Refeicao> refeicoes = List();
      QuerySnapshot res = await Firestore.instance.collection('instituicoes/$instituicaoDocID/refeitorios/$refeitorioDocID/refeicao').getDocuments();
      for(var doc in res.documents) {
        Refeicao refeicao = Refeicao.fromJson(doc.data, doc.documentID);
        refeicoes.add(refeicao);
      }
      return refeicoes;
    } catch (e) {
      return null;
    }
  }
 
  static Future<bool> editDiaSemana(String instituicaoDocID, String refeitorioDocId, String refeicaoDocId, int dia, DateTime inicio, DateTime fim, DateTime max)async{
    bool retorno;
    try {
      Map<String, dynamic> mapDias;
      switch (dia) {
        case 0:
          mapDias = {
            "segInicio": inicio,
            "segFim": fim,
            "segMax": max
          };
          break;
        case 1:
          mapDias = {
            "terInicio": inicio,
            "terFim": fim,
            "terMax": max
          };
          break;
        case 2:
          mapDias = {
            "quaInicio": inicio,
            "quaFim": fim,
            "quaMax": max
          };
          break;
        case 3:
          mapDias = {
            "quiInicio": inicio,
            "quiFim": fim,
            "quiMax": max
          };
          break;
        case 4:
          mapDias = {
            "sexInicio": inicio,
            "sexFim": fim,
            "sexMax": max
          };
          break;
        case 5:
          mapDias = {
            "sabInicio": inicio,
            "sabFim": fim,
            "sabMax": max
          };
          break;
        case 6:
          mapDias = {
            "domInicio": inicio,
            "domFim": fim,
            "domMax": max
          };
          break;
      }
      String docIdRes;
      if(refeicaoDocId == null) {
        var resAdd = await Firestore.instance.collection('instituicoes/$instituicaoDocID/refeitorios/$refeitorioDocId/refeicao').add(Refeicao.nulo().toJson());
        docIdRes = resAdd.documentID;
      } else {
        docIdRes = refeicaoDocId;
      }
      await Firestore.instance.collection('instituicoes/$instituicaoDocID/refeitorios/$refeitorioDocId/refeicao').document(docIdRes).setData(mapDias, merge: true);
      retorno=true;
    } catch (e) {
      print(e.toString());
      retorno=false;
    }
    return retorno;
  }


}