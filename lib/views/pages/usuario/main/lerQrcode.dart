import 'dart:async';
import 'package:flutter/material.dart';
import 'package:fast_qr_reader_view/fast_qr_reader_view.dart';
import 'package:ms2019_2/models/vinculo.dart';
import 'package:ms2019_2/resources/singleton.dart';
import 'package:ms2019_2/resources/usuarioServ.dart';



class LerQrCode extends StatefulWidget {
  final List<CameraDescription> cameras;
  LerQrCode(this.cameras);
  @override
  _LerQrCodeState createState() => new _LerQrCodeState();
}

class _LerQrCodeState extends State<LerQrCode> {
  QRReaderController controller;

  @override
  void initState() {
    super.initState();
    controller = new QRReaderController(widget.cameras[0], ResolutionPreset.medium, [CodeFormat.qr], (dynamic value)async{
        if(value.runtimeType == String){
          if(Singleton.getInstance().usuario.vinculos.contains(value)) {
            Navigator.of(context).pop(0);
          } else {
            List<Vinculo> vinculos = await UsuarioServ.getVinculos([value]);
            if(vinculos.isNotEmpty) {
              Singleton.getInstance().usuario.vinculos.add(value);
              await UsuarioServ.editUsuario(Singleton.getInstance().usuario);
              Navigator.of(context).pop(1);
            } else {
              Navigator.of(context).pop(2);
            }
          } 
        }
    new Future.delayed(const Duration(seconds: 3), controller.startScanning);
    });
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
      controller.startScanning();
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!controller.value.isInitialized) {
      return new Container();
    }
    return new AspectRatio(
        aspectRatio:
        controller.value.aspectRatio,
        child: new QRReaderPreview(controller));
  }
}