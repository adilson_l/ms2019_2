import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ms2019_2/models/cardapio.dart';
import 'package:ms2019_2/models/checkin.dart';

class CardapioServ{

  static Future<String> addCardapio(Cardapio cardapio,String instituicaoDocID, String refeitorioDocID, String refeicaoDocId)async{
    String retorno;
    try {
      var doc = await Firestore.instance.collection('instituicoes/$instituicaoDocID/refeitorios/$refeitorioDocID/refeicao/$refeicaoDocId/cardapio').add(cardapio.toJson());
      retorno=doc.documentID;
    } catch (e) {
      retorno=e.toString();
    }
    return retorno;
  }

   static Future<bool> editCardapio(Cardapio cardapio,String instituicaoDocID, String refeitorioDocID, String refeicaoDocId)async{
    bool retorno;
    try {
      await Firestore.instance.collection('instituicoes/$instituicaoDocID/refeitorios/$refeitorioDocID/refeicao/$refeicaoDocId/cardapio').document(cardapio.docID).updateData(cardapio.toJson());
      retorno=true;
    } catch (e) {
      print(e.toString());
      retorno=false;
    }
    return retorno;
  }
    static Future<bool> deleteCardapio(Cardapio cardapio,String instituicaoDocID, String refeitorioDocID, String refeicaoDocId)async{
    bool retorno;
    try {
      await Firestore.instance.collection('instituicoes/$instituicaoDocID/refeitorios/$refeitorioDocID/refeicao/$refeicaoDocId/cardapio').document(cardapio.docID).delete();
      retorno=true;
    } catch (e) {
      print(e.toString());
      retorno=false;
    }
    return retorno;
  }

  static Future<List<Cardapio>> getCardapios(String instituicaoDocID, String refeitorioDocID, String refeicaoDocId) async {
    try {
      QuerySnapshot res = await Firestore.instance.collection("instituicoes/$instituicaoDocID/refeitorios/$refeitorioDocID/refeicao/$refeicaoDocId/cardapio").getDocuments();
      List<Cardapio> cardapios = List();
      for (var doc in res.documents) {
        cardapios.add(Cardapio.fromJson(doc.data, doc.documentID));
      }
      return cardapios;
    } catch (e) {
      print(e.toString());
      return List();
    }
  }

  static Future<Checkin> getCheckin(String instituicaoDocID, String refeitorioDocID, String refeicaoDocId, String cardapioDocId, String usuarioDocId) async {
    try {
      QuerySnapshot res = await Firestore.instance.collection("instituicoes/$instituicaoDocID/refeitorios/$refeitorioDocID/refeicao/$refeicaoDocId/cardapio/$cardapioDocId/checkins").getDocuments();
      Checkin checkin = Checkin.nulo();
      for (var doc in res.documents) {
        if(doc.data["usuarioId"] == usuarioDocId) {
          checkin = Checkin.fromJson(doc.data, doc.documentID);
        }
      }
      return checkin;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  static Future<void> editCheckin(String instituicaoDocID, String refeitorioDocID, String refeicaoDocId, String cardapioDocId, String checkinDocId, bool checkin) async {
    try {
      await Firestore.instance.document("instituicoes/$instituicaoDocID/refeitorios/$refeitorioDocID/refeicao/$refeicaoDocId/cardapio/$cardapioDocId/checkins/$checkinDocId").updateData({"checkin": checkin});
    } catch (e) {
      print(e.toString());
    }
  }
}