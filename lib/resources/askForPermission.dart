 import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class AskForPermission extends StatefulWidget {
  @override
  _AskForPermissionState createState() => _AskForPermissionState();
}

class _AskForPermissionState extends State<AskForPermission> {
  Map<PermissionGroup, PermissionStatus> permissions;
  @override
  void initState(){
    super.initState();
    getPermission().then((_) {
      Navigator.pop(context);
    });
  }

  Future<void> getPermission() async {
    permissions = await PermissionHandler().requestPermissions([
      PermissionGroup.camera,
      PermissionGroup.sensors,
      PermissionGroup.storage
    ]);


  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Text("Permissões Necessárias")
        ),
      
    );
  }
}