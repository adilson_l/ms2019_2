class Refeitorio {
  String docID;
  String descricao;
  String localizacao;
  String codigoQR;

  Refeitorio(
    this.descricao,
    this.localizacao,
    {this.codigoQR}
  );
  Refeitorio.nulo() {
    docID = descricao = localizacao = codigoQR = null;
  }

  Refeitorio.fromJson(Map<String, dynamic> dados, String _docID) {
    docID = _docID;
    descricao = dados['descricao'];
    localizacao = dados['localizacao'];
    codigoQR = dados['codigoQR'];
  }

  Map<String, dynamic> toJson() => {
        "descricao": descricao,
        "localizacao": localizacao,
        "codigoQR": codigoQR
      };
}
