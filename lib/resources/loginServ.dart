import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ms2019_2/resources/instituicaoServ.dart';
import 'package:ms2019_2/resources/singleton.dart';
import 'package:ms2019_2/resources/usuarioServ.dart';
import 'package:ms2019_2/models/instituicao.dart';
import 'package:ms2019_2/models/usuario.dart';
import 'package:ms2019_2/views/pages/usuario/main/usuarioMainPage.dart';

class LoginServ {
  static Future<bool> signInWithEmailUsuario(BuildContext context, String _email,
      String _senha, Usuario _usuario, GlobalKey<ScaffoldState> _scaffoldKey) async {
    bool logou = false;
    try {
      AuthResult result = await FirebaseAuth.instance.createUserWithEmailAndPassword(email: _email, password: _senha);
      print(result);
      Navigator.of(context).pop();
      logou = true;
      await UsuarioServ.addUsuario(_usuario);
      Singleton.getInstance().usuario=_usuario;
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => UsuarioMainPage()),
          (Route<dynamic> route) => false);
    } catch (e) {
      String erro;
      Navigator.of(context).pop();
      switch (e.code) {
        case "ERROR_WEAK_PASSWORD":
          erro = "Senha muito fraca";
          break;
        case "ERROR_INVALID_EMAIL":
          erro = "E-mail inválido";
          break;
        case "ERROR_EMAIL_ALREADY_IN_USE":
          erro = "E-mail já vinculado em outra conta";
          break;
        default:
          erro = e.message;
      }
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(erro)
      ));
    }
    return logou;
  }

  static Future<bool> logInWithEmailUsuario(BuildContext context, String _email,
      String _senha, GlobalKey<ScaffoldState> _scaffoldKey) async {
    bool logou;
    try {
      Usuario usuario = await UsuarioServ.getUsuario(_email);
      if(usuario != null) {
        await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: _email, password: _senha);
        Navigator.of(context).pop();
        logou = true;
        Singleton.getInstance().usuario = usuario;

      } else {
        logou = false;
        _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Usuário inválido")
      ));
      }
    } catch (e) {
      String erro;
      logou = false;
      switch (e.code) {
        case "ERROR_USER_NOT_FOUND":
          erro = "Usuário inválido";
          break;
        case "ERROR_WRONG_PASSWORD":
          erro = "Senha inválida";
          break;
        case "ERROR_USER_DISABLE":
          erro = "Usuário desativado";
          break;
        case "ERROR_USER_TOKEN_EXPIRED":
          erro = "Sessão expirada";
          break;
        default:
          erro = e.message;
      }
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(erro)
      ));
    }
    return logou;
  }

  static Future<bool> signInWithEmailInstituicao(BuildContext context, String _email,
      String _senha, Instituicao _instituicao, GlobalKey<ScaffoldState> _scaffoldKey) async {
    bool logou = false;
    try {
      AuthResult result = await FirebaseAuth.instance.createUserWithEmailAndPassword(email: _email, password: _senha);
      print(result);
      Navigator.of(context).pop();
      logou = true;
      Singleton.getInstance().instituicao=_instituicao;
      await InstituicaoServ.addInstituicao(_instituicao);
      
    } catch (e) {
      String erro;
      
      switch (e.code) {
        case "ERROR_WEAK_PASSWORD":
          erro = "Senha muito fraca";
          break;
        case "ERROR_INVALID_EMAIL":
          erro = "E-mail inválido";
          break;
        case "ERROR_EMAIL_ALREADY_IN_USE":
          erro = "E-mail já vinculado em outra conta";
          break;
        default:
          erro = e.message;
      }
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(erro)
      ));
    }
    return logou;
  }

  static Future<bool> logInWithEmailInstituicao(BuildContext context, String _email,
      String _senha, GlobalKey<ScaffoldState> _scaffoldKey) async {
    bool logou;
    try {
      Instituicao instituicao = await InstituicaoServ.getInstituicao(_email);
      if(instituicao != null) {
        await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: _email, password: _senha);
        Navigator.of(context).pop();
        logou = true;
        Singleton.getInstance().instituicao = instituicao;
 
      } else {
        logou = false;
        _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Instituição inválida")
      ));
      }
    } catch (e) {
      String erro;
      logou = false;
      switch (e.code) {
        case "ERROR_USER_NOT_FOUND":
          erro = "Usuário inválido";
          break;
        case "ERROR_WRONG_PASSWORD":
          erro = "Senha inválida";
          break;
        case "ERROR_USER_DISABLE":
          erro = "Usuário desativado";
          break;
        case "ERROR_USER_TOKEN_EXPIRED":
          erro = "Sessão expirada";
          break;
        default:
          erro = e.message;
      }
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(erro)
      ));
    }
    return logou;
  }
}