import 'package:cloud_firestore/cloud_firestore.dart';

class Cardapio {
  String docID;
  bool ativo;
  String descricao;
  DateTime data;
  String obs;
  String foto;

  Cardapio(
    this.ativo,
    this.descricao,
    this.data,
    this.obs,
    this.docID,
    this.foto
  );

  Cardapio.nulo() {
    ativo = descricao = data = obs = docID = foto = null;
  }

  Cardapio.fromJson(Map<String,dynamic> dados, String _docID){
    docID=_docID;
    ativo=dados['ativo'];
    descricao=dados['descricao'];
    data=dados['data'] != null ? dados['data'].toDate() : null;
    obs=dados['obs'];
    foto=dados['foto'];
  }
  

  Map<String, dynamic> toJson() => {
        "ativo": ativo,
        "descricao": descricao,
        "data": Timestamp.fromDate(data),
        "obs": obs,
        "foto": foto
      };
}
