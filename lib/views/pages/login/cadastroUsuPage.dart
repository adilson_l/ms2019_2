import 'package:flutter/material.dart';
import 'package:ms2019_2/resources/loginServ.dart';
import 'package:ms2019_2/models/usuario.dart';
import 'package:ms2019_2/views/components/fundoGradienteCold.dart';

class CadastroUsuarioPage extends StatefulWidget {
  @override
  _CadastroUsuarioPageState createState() => _CadastroUsuarioPageState();
}

class _CadastroUsuarioPageState extends State<CadastroUsuarioPage> {
  GlobalKey<FormState> _key = GlobalKey();
  GlobalKey<ScaffoldState> _scafoldkey=GlobalKey();
  TextEditingController nomeController= TextEditingController();
  TextEditingController senhaController= TextEditingController();
  TextEditingController senha2Controller= TextEditingController();
  TextEditingController cpfController= TextEditingController();
  TextEditingController emailController= TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scafoldkey,
     body: Stack(
       children: <Widget>[
         FundoGradienteCold(),
         Center(
              child: Form(
                key: _key,
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Text('Cadastrar',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 32
                      ),),
                      Container(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.05),
                        child: TextFormField(
                          style: TextStyle(color: Colors.white),
                          controller: nomeController,
                          validator: (value){
                            if(value==null || value.isEmpty){
                              return 'Campo inválido';
                            }else{
                              return null;
                            }
                          },
                          decoration: InputDecoration(
                            labelText: 'Nome',
                            labelStyle:TextStyle(color: Colors.white),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white
                              )
                            ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white))),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.01),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.05),
                        child: TextFormField(
                          style: TextStyle(color: Colors.white),
                          controller: emailController,
                          validator: (String valor) {
                    Pattern pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regex = new RegExp(pattern);
                    if (!regex.hasMatch(valor))
                      return 'Digite um e-mail válido';
                    else
                      return null;
                  },
                          decoration: InputDecoration(
                            labelText: 'Email',
                            labelStyle:TextStyle(color: Colors.white),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white
                              )
                            ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white))),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.01),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.05),
                        child: TextFormField(
                          controller: cpfController,
                           validator: (value){
                            if(value==null || value.isEmpty){
                              return 'Campo inválido';
                            }else{
                              return null;
                            }
                          },
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                            labelText: 'CPF',
                            labelStyle:TextStyle(color: Colors.white),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white
                              )
                            ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white))),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.01),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.05),
                        child: TextFormField(
                          style: TextStyle(color: Colors.white),
                           validator: (value){
                            if(value==null || value.isEmpty|| value.length<6){
                              return 'Campo inválido';
                            }else{
                              return null;
                            }
                          },
                          controller: senhaController,
                          obscureText: true,
                          decoration: InputDecoration(
                            labelText: 'Senha',
                            labelStyle:TextStyle(color: Colors.white),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white
                              )
                            ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white))),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.01),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.05),
                        child: TextFormField(
                          controller: senha2Controller,
                           validator: (value){
                            if(value==null || value.isEmpty || value.length<6){
                              return 'Campo inválido';
                            }else{
                              return null;
                            }
                          },
                          obscureText: true,
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                            labelText: 'Repetir Senha',
                            labelStyle:TextStyle(color: Colors.white),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white
                              )
                            ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white))),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width*0.9,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(15.0)),
                          color: Colors.white,
                          child: Text("Cadastrar",
                          style: TextStyle(color: Colors.blueAccent),),
                          onPressed: ()async{
                            if(_key.currentState.validate()){
                            Usuario usuario= Usuario(cpfController.text, emailController.text, nomeController.text, []);
                            await LoginServ.signInWithEmailUsuario(context, emailController.text, senhaController.text, usuario, _scafoldkey);
                            }else{
                                _scafoldkey.currentState.showSnackBar(SnackBar(
                                content: Text('Inválido'),
                              ));
                            }
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
       ],
     ),
      
    ),
    );
  }
  
}