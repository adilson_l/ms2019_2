import 'package:flutter/material.dart';


class FundoGradienteCold extends StatefulWidget {
  @override
  _FundoGradienteColdState createState() => _FundoGradienteColdState();
}

class _FundoGradienteColdState extends State<FundoGradienteCold> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.blue,Colors.blue[200],Colors.teal[200],Colors.teal],
                begin: Alignment.bottomLeft,
                end: Alignment.topRight,
              )
            ),
    );
  }
}