import 'package:ms2019_2/models/instituicao.dart';
import 'package:ms2019_2/models/usuario.dart';



class Singleton {
  static Singleton _instance;
  Usuario usuario;
  Instituicao instituicao;
  bool conectado;
  

  static Singleton getInstance(){
      if(_instance == null)
        _instance = Singleton();
      return _instance;
  }

}

