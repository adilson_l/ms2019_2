import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:ms2019_2/models/cardapio.dart';
import 'package:ms2019_2/models/refeicao.dart';
import 'package:ms2019_2/models/refeitorio.dart';
import 'package:ms2019_2/resources/singleton.dart';

class CheckinPage extends StatefulWidget {
  final Cardapio cardapio;
  final Refeitorio refeitorio;
  final Refeicao refeicao;
  CheckinPage(this.cardapio,this.refeitorio,this.refeicao);
  @override
  _CheckinPageState createState() => _CheckinPageState();
}

class _CheckinPageState extends State<CheckinPage> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Check-in'),
      ),
      body: StreamBuilder(
        stream: Firestore.instance.collection('instituicoes/${Singleton.getInstance().instituicao.docID}/refeitorios/${widget.refeitorio.docID}/refeicao/${widget.refeicao.docID}/cardapio/${widget.cardapio.docID}/checkins').snapshots(),
        builder: (context,snap){
          switch (snap.connectionState) {
            case ConnectionState.waiting:
            return CircularProgressIndicator();
            break;
            default:
            return ListView.builder(
              itemCount: snap.data.documents.length,
              itemBuilder: (conte,i){
                return Padding(
                  padding: EdgeInsets.symmetric(vertical: 8),
                  child: ListTile(
                    title:Card(
                      color: Colors.deepOrange,
                      child:  Padding(
                        padding: EdgeInsets.all(8),
                        child: Text(snap.data.documents[i]['usuarioNome'],style:TextStyle(color: Colors.white),),
                      ),
                    ),
                ),
                );
              },
            );
          }
        },
      ),

      
    );
  }
}