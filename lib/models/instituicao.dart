

class Instituicao{
  String docID;
  String cep;
  String cidade;
  String cnpj;
  String email;
  String estado;
  String logadouro;
  String nome;
  String numero;

  Instituicao.nulo(){
    cep=cidade=cnpj=email=estado=logadouro=nome=numero=null;
  }

  Instituicao(this.nome,this.email,this.cidade,
  this.numero,this.logadouro,this.cep,this.cnpj,this.estado);

  Instituicao.fromJson(Map<String,dynamic> dados, String _docID){
    docID=_docID;
    cep=dados['cep'];
    cidade=dados['cidade'];
    cnpj=dados['cnpj'];
    email=dados['email'];
    estado=dados['estado'];
    logadouro=dados['logadouro'];
    numero=dados['numero'];
    nome=dados['nome'];
  }

  Map<String,dynamic> toJson(){
    return <String,dynamic>{
    'cep':cep,
    'cidade':cidade,
    'cnpj':cnpj,
    'email':email,
    'estado':estado,
    'logadouro':logadouro,
    'numero':numero,
    'nome':nome,
      
    };
  }


}