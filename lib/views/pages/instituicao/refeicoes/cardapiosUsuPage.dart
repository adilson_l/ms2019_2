import 'package:flutter/material.dart';
import 'package:ms2019_2/models/cardapio.dart';
import 'package:ms2019_2/models/checkin.dart';
import 'package:ms2019_2/models/refeicao.dart';
import 'package:ms2019_2/models/refeitorio.dart';
import 'package:ms2019_2/resources/cardapioServ.dart';
import 'package:ms2019_2/resources/checkinServ.dart';
import 'package:ms2019_2/resources/singleton.dart';
import 'package:table_calendar/table_calendar.dart';

class CardapiosPageUsu extends StatefulWidget {
  final Refeitorio refeitorio;
  final String instituicaoDocId;
  final Refeicao refeicao;

  CardapiosPageUsu(this.refeitorio, this.instituicaoDocId, this.refeicao);

  @override
  _CardapiosPageUsuState createState() => _CardapiosPageUsuState();
}

class _CardapiosPageUsuState extends State<CardapiosPageUsu> {
  bool _isBusy = false;
  bool _isBusyClick = false;
  Checkin _checkin = Checkin.nulo();
  Map<DateTime, List> _eventos = Map();
  List<Cardapio> _cardapios = List();
  Cardapio _selecionado;
  DateTime _dataSelecionada;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  CalendarController calendarController = CalendarController();

  @override
  void initState() {
    _isBusy = true;
    CardapioServ.getCardapios(widget.instituicaoDocId, widget.refeitorio.docID,
            widget.refeicao.docID)
        .then((cardapios) {
      _cardapios = cardapios;
      for (Cardapio cardapio in _cardapios) {
        if (_eventos[cardapio.data] == null) _eventos[cardapio.data] = List();
        _eventos[cardapio.data].add(cardapio);
      }
      _carregarEventos(DateTime.now());
      _isBusy = false;
      setState(() {});
    });
    super.initState();
  }

  Future<void> _carregarEventos(DateTime dia) async {
    bool entrou = false;
    _eventos.forEach((data, listaCardapio) {
      if (data.day == dia.day &&
          data.month == dia.month &&
          data.year == dia.year) {
        _selecionado = listaCardapio.first;
        entrou = true;
      }
      _dataSelecionada = dia;
    });
    if (!entrou) {
      _selecionado = null;
      _checkin = Checkin.nulo();
    } else {
      _checkin = await CardapioServ.getCheckin(widget.instituicaoDocId, widget.refeitorio.docID, widget.refeicao.docID, _selecionado.docID, Singleton.getInstance().usuario.docID);
      print("teste");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: Text("Cardápios")),
      body: _isBusy
          ? Center(child: CircularProgressIndicator())
          : Column(
              children: <Widget>[
                TableCalendar(
                  calendarController: calendarController,
                  locale: 'pt_BR',
                  events: _eventos,
                  availableCalendarFormats: const {
                    CalendarFormat.month: 'Mês',
                    CalendarFormat.twoWeeks: '2 semanas',
                    CalendarFormat.week: 'Semana',
                  },
                  calendarStyle: CalendarStyle(
                    selectedColor: Theme.of(context).primaryColor,
                    todayColor: Color(0xFFb1c3cc),
                    markersColor: Colors.black,
                  ),
                  onDaySelected: (date, events) async {
                    _isBusyClick = true;
                    setState(() {});
                    await _carregarEventos(date);
                    _isBusyClick = false;
                    setState(() {});
                  },
                ),
                Expanded(
                    child: _isBusyClick ? Center(child: CircularProgressIndicator()) :SingleChildScrollView(
                  child: Container(
                      padding: EdgeInsets.all(
                          MediaQuery.of(context).size.width * 0.02),
                      child: _selecionado != null
                          ? _selecionado.ativo
                              ? Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Text("Descrição: ",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16.0)),
                                        Text(_selecionado.descricao,
                                            style: TextStyle(fontSize: 15.0))
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Text("Observação: ",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16.0)),
                                        Text(_selecionado.obs,
                                            style: TextStyle(fontSize: 15.0))
                                      ],
                                    ),
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Container(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.5,
                                        height: MediaQuery.of(context).size.width * 0.35,
                                        padding: EdgeInsets.only(
                                            top: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.02),
                                        child: Image.network(
                                          _selecionado.foto != null
                                              ? _selecionado.foto
                                              : "https://i.pinimg.com/736x/a6/93/de/a693deeb3c809a0786e98b7f19421829.jpg",
                                          scale: 5,
                                          fit: BoxFit.fitWidth,
                                        )),
                                        Visibility(
                                          visible: true,
                                          child: Column(
                                            children: <Widget>[
                                              Text("Check-in", style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold)),
                                              Row(children: <Widget>[
                                                Visibility(
                                                  visible: _checkin.checkin == null || !_checkin.checkin,
                                                  child: IconButton(
                                                    icon: Icon(Icons.check, color: Colors.green, size: 30.0,),
                                                    onPressed: () async {
                                                      Checkin checkin = Checkin(
                                                          true,
                                                          Singleton.getInstance()
                                                              .usuario
                                                              .docID,
                                                          Singleton.getInstance()
                                                              .usuario
                                                              .nome);
                                                      String result;
                                                      if(_checkin.usuarioId == null) {
                                                        result = await CheckinServicos.addCheckin(
                                                                widget.refeitorio.docID,
                                                                widget.instituicaoDocId,
                                                                _selecionado.docID,
                                                                widget.refeicao.docID,
                                                                checkin);
                                                      } else {
                                                        result = "";
                                                        await CardapioServ.editCheckin(
                                                                widget.instituicaoDocId,
                                                                widget.refeitorio.docID,
                                                                widget.refeicao.docID,
                                                                _selecionado.docID,
                                                                _checkin.docId,
                                                                true);
                                                      }
                                                      _checkin.checkin = true;
                                                      if (result != null) {
                                                        _scaffoldKey.currentState
                                                            .showSnackBar(SnackBar(
                                                          content: Text("Check-in feito"),
                                                        ));
                                                      }
                                                      setState(() {});
                                                    },
                                                  ),
                                                ),
                                                Visibility(
                                                  visible: _checkin.checkin == null || _checkin.checkin,
                                                  child: IconButton(
                                                    icon: Icon(Icons.close, color: Colors.red, size: 30.0,),
                                                    onPressed: () async {
                                                      Checkin checkin = Checkin(
                                                          false,
                                                          Singleton.getInstance()
                                                              .usuario
                                                              .docID,
                                                          Singleton.getInstance()
                                                              .usuario
                                                              .nome);
                                                      String result;
                                                      if(_checkin.usuarioId == null) {
                                                        result = await CheckinServicos.addCheckin(
                                                                widget.refeitorio.docID,
                                                                widget.instituicaoDocId,
                                                                _selecionado.docID,
                                                                widget.refeicao.docID,
                                                                checkin);
                                                      } else {
                                                        result = "";
                                                        await CardapioServ.editCheckin(
                                                                widget.instituicaoDocId,
                                                                widget.refeitorio.docID,
                                                                widget.refeicao.docID,
                                                                _selecionado.docID,
                                                                _checkin.docId,
                                                                false);
                                                      }
                                                      _checkin.checkin = false;
                                                      if (result != null) {
                                                        _scaffoldKey.currentState
                                                            .showSnackBar(SnackBar(
                                                          content: Text("Check-in cancelado"),
                                                        ));
                                                      }
                                                      setState(() {});
                                                    },
                                                  ),
                                                )
                                              ])
                                            ],
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                )
                              : Center(
                                  child: Text('Refeição indisponível neste dia',
                                      style: TextStyle(fontSize: 15.0))
                                )
                          : Center(
                              child: Column(
                                children: <Widget>[
                                  Text('Ainda não há cardápio',
                                      style: TextStyle(fontSize: 15.0))
                                ],
                              ),
                            )),
                )),
              ],
            ),
    );
  }
}
